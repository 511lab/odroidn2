#!/bin/bash
BUIDDIR=./builds
export KBUILD_OUTPUI=$BUILDDIR
export PATH=/src/toolchain/aarch64-elf/bin:$PATH
export CROSS_COMPILE=aarch64-elf-
export ARCH=arm64
cd linux-odroidn2-4.9.y
make distclean
make odroidn2_defconfig
#make modprepare
make menuconfig 
make -j4

