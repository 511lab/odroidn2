#!/bin/bash

export PATH=/src/toolchain/arm-eabi/bin:/src/toolchain/aarch64-elf/bin:$PATH
export CROSS_COMPILE=aarch64-elf-
export ARCH=arm64
cd u-boot
make distclean
make odroidn2_defconfig
#make modprepare
#make menuconfig 
make -j5 V=1
 

